#!/bin/bash
isitfree () { [[ $page =~ 'name your price' || $page =~ 'Free Download' ]]; }
defpage () { page=$(elinks -anonymous 1 -dump $1); }
addfree () { f=$(($f+$1)); }
read -p 'Input bandcamp discography to search: ' lonk
links=( $(elinks -anonymous 1 -dump $lonk | grep 'https' | grep 'album\|track' | grep -v help/downloading? | sed 's/.* //' | awk '!seen[$0]++') )
excl='? #lyrics .com/album'
f=0
t=0
echo 'Free links:'
for (( i=0; i<${#links[@]}; i++ )); do
    if [[ "${links[$i]}" == *"album"* ]] ; then
        defpage ${links[$i]}
        readarray -t linknums < <(echo "$page" | sed -e 's/^[ \t]*//' | sed -n '/Share \/ Embed/,/^about$/{/Share \/ Embed/b;/^about$/b;p}' | grep -o "\[[0-9]*\]" | grep -o "[0-9]*")
        alinks=( $(echo "$page" | sed -n "/$linknums/,/${linknums[-1]}/p" | grep -o "[0-9].*" | grep 'https.*track' | grep -v ${excl// /\\|} | grep -oE '[^ ]+$' | uniq) )
        t=$(($t+${#alinks[@]}))
        if isitfree; then
            echo ${links[$i]}; addfree ${#alinks[@]}
        else
            for (( a=0; a<${#alinks[@]}; a++ )); do
                defpage ${alinks[$a]}
                if isitfree; then
                    echo ${alinks[$a]}; addfree 1
                fi
            done
        fi
    else
        defpage ${links[$i]}
        if isitfree; then 
            echo ${links[$i]}; addfree 1
        fi
    fi
done
echo "$(expr 100 \* $f / $t)% free"
